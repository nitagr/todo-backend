import {
  Controller,
  Res,
  HttpStatus,
  Post,
  Body,
  Get,
  Patch,
  Param,
  Delete,
  Req,
  UseGuards,
} from '@nestjs/common';
import { TodoService } from './todo.service';
import { AddTodoDto } from './dto/todo.dto';
import { Todo } from './interface/todo.interface';
import { Request, Response } from 'express';
import { JwtAuthGuard } from '../auth/jwt-auth.gaurd';
import { UsersService } from 'src/users/users.service';

@Controller('todo')
export class TodoController {
  constructor(
    private todoService: TodoService,
    private usersService: UsersService,
  ) {}

  // controller for adding a todo, which uses todoservice class
  // for adding a todo to DB. It also uses JwtAuthGaurd to vefify the token
  // inside the cookie of request
  @Post('add')
  @UseGuards(JwtAuthGuard)
  async addTodo(
    @Res() res: Response,
    @Req() req: Request,
    @Body() addTodoDto: AddTodoDto,
  ) {
    try {
      const userEmail = req.cookies['email'];
      addTodoDto.email = userEmail;
      const todo = await this.todoService.addTodo(addTodoDto);
      return res.status(HttpStatus.CREATED).json({
        status: 201,
        message: 'success',
        data: todo,
      });
    } catch (err) {
      return res.status(HttpStatus.UNPROCESSABLE_ENTITY).json({
        status: err.status,
        message: err.message,
      });
    }
  }

  // controller for retrieving all todos based on the user, which uses todoservice class
  // for getting all todos from the DB. It also uses JwtAuthGaurd to vefify the
  // token inside the cookie of request
  @Get('all')
  @UseGuards(JwtAuthGuard)
  async getAllTodos(@Res() res: Response, @Req() req: Request) {
    try {
      const email = req.cookies['email'];
      const todos: Todo[] = await this.todoService.getAllTodos(email);
      return res.status(HttpStatus.OK).json({
        status: 200,
        message: 'success',
        data: todos,
      });
    } catch (err) {
      return res.status(HttpStatus.UNPROCESSABLE_ENTITY).json({
        status: err.status,
        message: err.message,
      });
    }
  }

  // controller for updating a todo by its ID, which uses todoservice class
  // for updating a todo in the DB. It also uses JwtAuthGaurd to vefify the token
  // inside the cookie of request
  @Patch(':id')
  @UseGuards(JwtAuthGuard)
  async updateTodoById(
    @Res() res: Response,
    @Body() addTodoDto: AddTodoDto,
    @Param() params,
  ) {
    try {
      const todo: Todo = await this.todoService.updateTodoById(
        params.id,
        addTodoDto,
      );
      if (!todo) {
        return res
          .status(HttpStatus.NOT_FOUND)
          .json({ status: 404, error: 'Not found!' });
      }

      return res.status(HttpStatus.OK).json({
        status: 200,
        message: 'success',
        data: todo,
      });
    } catch (err) {
      return res.status(HttpStatus.UNPROCESSABLE_ENTITY).json({
        status: err.status,
        message: err.message,
      });
    }
  }

  // controller for deleting a todo by its ID, which uses todoservice class
  // for deleting a todo in the DB. It also uses JwtAuthGaurd to vefify the token
  // inside the cookie of request
  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  async deleteTodoById(
    @Res() res: Response,
    @Req() req: Request,
    @Param() params,
  ) {
    try {
      const todo = await this.todoService.deleteTodoById(params.id);
      if (!todo) {
        return res
          .status(HttpStatus.NOT_FOUND)
          .json({ status: 404, error: 'Not found!' });
      }

      return res.status(HttpStatus.OK).json({
        status: 200,
        message: 'success',
        data: todo,
      });
    } catch (err) {
      return res.status(HttpStatus.UNPROCESSABLE_ENTITY).json({
        status: err.status,
        message: err.message,
      });
    }
  }

  // controller for deleting all the todos, which uses todoservice class
  // for deleting all todos in the DB. It also uses JwtAuthGaurd to vefify the token
  // inside the cookie of request
  @Delete('')
  @UseGuards(JwtAuthGuard)
  async deleteAllTodos(@Res() res: Response) {
    try {
      const todo = await this.todoService.deleteAllTodos();
      if (!todo) {
        return res
          .status(HttpStatus.NOT_FOUND)
          .json({ status: 404, error: 'Not found!' });
      }

      return res.status(HttpStatus.OK).json({
        status: 200,
        message: 'success',
        data: todo,
      });
    } catch (err) {
      return res.status(HttpStatus.UNPROCESSABLE_ENTITY).json({
        status: err.status,
        message: err.message,
      });
    }
  }
}
