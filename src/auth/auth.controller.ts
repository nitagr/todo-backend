import { Controller, Post, Body, HttpStatus, Res } from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginUserDto } from '../users/dto/login-user.dto';
import { Response } from 'express';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  // Login controller which takes user email and password
  // and verifies the user, after which it assigns a token in
  // cookie, which makes authenticated requests to protected
  // routes
  @Post('login')
  async login(
    @Res({ passthrough: true }) res: Response,
    @Body() loginUserDto: LoginUserDto,
  ) {
    try {
      const data = await this.authService.validateUserByPassword(loginUserDto);
      if (data) {
        res.cookie('token', data.token, { httpOnly: true, maxAge: 1800000 });
        res.cookie('email', data.email, { httpOnly: true, maxAge: 1800000 });
        return res.status(HttpStatus.OK).json({
          status: 201,
          message: 'success',
          data: data,
        });
      }
      return res.status(HttpStatus.UNAUTHORIZED).json({
        status: 401,
        message: 'Email or Password is incorrect',
      });
    } catch (err) {
      console.log(err);
      return;
    }
  }

  // Logout controller which clears the cookie and makes the
  // user unauthenticated
  @Post('logout')
  async logout(@Res({ passthrough: true }) res: Response) {
    try {
      res.clearCookie('token');
      res.clearCookie('email');
      return res.status(HttpStatus.OK).json({
        status: 201,
        message: 'success',
      });
    } catch (err) {
      console.log(err);
      return;
    }
  }
}
